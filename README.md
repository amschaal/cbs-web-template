# UC Davis College of Biological Sciences Web Templates

The [College of Biological Sciences](http://biosci.ucdavis.edu) (CBS), in order to align both the visual footprint, informational paradigm, and technological backbone of the college, provides these html & WordPress templates for use and adaptation. 

## Directions for Use

The templates are available for download at [https://bitbucket.org/ucdavis/cbs-web-template](https://bitbucket.org/ucdavis/cbs-web-template). For those using version control software, the Git repository is hosted at [http://bitbucket.org](http://bitbucket.org) and can be downloaded by running:

    $ git clone https://bitbucket.org/ucdavis/cbs-web-template.git
    
Then, to get the required dependencies, run:

    $ git submodule init
    $ git submodule update

### HTML / CSS templates

In order to keep CBS look & feel implementation-agnostic, a master CSS file is provided, as well as example html templates suited to use as-is or as examples for your own development. You'll find the master CSS file in the `css` folder, and the html templates in the `html` folder. For more information on using CSS for site design, check out [https://developer.mozilla.org/en-US/docs/CSS](https://developer.mozilla.org/en-US/docs/CSS).

All the html templates are designed to be HTML5 compliant. The CSS is designed out-of-the-box to be *responsive*, meaning that as the viewport (your screen) changes dimensions, the content automatically adjusts to represent the best view of the information for you.

#### HTML template breakdown

* **home.main.html**: This is a default 'homepage', complete with a main header as well as a 'news' section and a subsection for secondary content. This template is a great starting-off point for your application.
* **home.heavy.html**: This homepage is designed for content-heavy or image-light applications. Use this template if your web application has a lot of text or sections that you'd like to have available on the homepage.
* **home.light.html**: This homepage is designed for a more content-light, sales-oriented, and/or image-heavy design.
* **article.html**: This design would be used for articles posted online. It is also useful for secondary pages within your site's design.

### WordPress templates

The College of Biological Sciences provides an official WordPress theme, located in `wordpress`. For more information on using WordPress, check out [http://wordpress.org/](http://wordpress.org/). Check out how to use themes in WordPress at [http://codex.wordpress.org/Using_Themes](http://codex.wordpress.org/Using_Themes).

You may also download the wordpress theme directly [here](https://bitbucket.org/ucdavis/cbs-web-template/raw/master/wordpress.zip).

#### Extending the WordPress theme
If you'd like to extend the functionality provided by this theme, we recommend using Child Themes based upon this theme. For more information, check out [http://codex.wordpress.org/Child_Themes](http://codex.wordpress.org/Child_Themes). Editing the files directly could produce unexpected results and limit your ability to update the theme or rollback gracefully. Using Child Themes, you keep your changes small and independent from the parent theme, allow the parent theme to be updated without impacting your site's design, and keep the ability to fallback to the parent theme on the fly should you need to.

#### WordPress template files

* **front-page.php**: This page is used as a static 'homepage' for a site, based on the `home.main.html` in the `html` folder.
* **home.php**: This page is used to render the Blog Posts Index, whether on the site front page or on a static page.
* **single.php**: This article page is based on the `article.html` template in the `html` folder. It is used to display posts.
* **index.php**: This file is the fallback for every other type of display and generally should not be edited. It's better to create a display file for your particular type of resource. See [http://codex.wordpress.org/Template_Hierarchy](http://codex.wordpress.org/Template_Hierarchy) for more information.
* **functions.php**: This file houses all the utility functions and setup files for the theme. You do not want to edit this file directly.

## Questions

If you have any questions on downloading, installing, and/or implementing these design templates, contact [Kimberly Bernick](mailto:kmbernick@ucdavis.edu).

## Acknowledgment

This project comes to you thanks to the vision and effort of James Hildreth, Dean of the College of Biological Sciences, as well as the efforts of [Donna Olsson](mailto:dwolsson@ucdavis.edu), Executive Assistant Dean and [Kimberly Bernick](mailto:kmbernick@ucdavis.edu), Chief Marketing & Communications officer. The templates were created by the [UC Davis Information and Educational Technology](http://iet.ucdavis.edu).
