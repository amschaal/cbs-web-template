from __future__ import with_statement
import os

#    Fabfile as a building aid to the resources, because fabric is better than make.

#    Requires you to have Python 2.7+ and fabric installed.

try: 
    from fabric.api import *
except ImportError, e:
    print """
    ERROR: Fabric is not installed.
    Try running 'pip install fabric' or using your package manager to install it.
    """

#    Setting up our directories
DIR = os.path.dirname(__file__)
BOOTSTRAP_DIR = os.path.join(DIR, "etc", "bootstrap")
BUILD_DIR = os.path.join(DIR, "build")
DEST_DIR = os.path.join(DIR, "html")
LESS_DIR = os.path.join(DIR, "less")
WP_DIR = os.path.join(DIR, "wordpress")
TEMPLATE = "cbs_wordpress"
RESOURCES = ['css', 'js', 'img']
BOOTSTRAP_RESOURCE_DIR = os.path.join(BOOTSTRAP_DIR, "bootstrap")

STANDARD_STYLE = 'style.less'
RESPONSIVE_STYLE = 'responsive.less'
COMPRESS = False
ALL = (
    STANDARD_STYLE, 
    RESPONSIVE_STYLE, 
    'mbc.less', 
    'home.less', 
    'article.less',
    'students.less',
)

@task(default = True)
def deploy_all(compress = COMPRESS):
    setup_environ()
    build_bootstrap()
    for ffile in ALL:
        build_file(ffile, compress)
    deploy()
    zip_wordpress(False)
    local('rm -r {0} ### cleaning up bootstrap'.format(os.path.join(BOOTSTRAP_RESOURCE_DIR)))

@task
def build_file(build_file, compress = COMPRESS):
    setup_environ()
    lessf, devf, minf, outf = proc_vars(build_file)
    with lcd(BUILD_DIR):
        local('recess --compile {0} > {1}'.format(os.path.join(LESS_DIR, lessf), devf))
        if compress == True or str(compress).lower() == 'true':
            local('recess --compress {0} > {1}'.format(devf, minf))
        else:
            minf = devf
        local('cat {0} {1} > {2} ### adding copyright to css'.format(
            os.path.join(DIR, 'copyright.txt'), minf, outf
        ))
        with settings(warn_only = True):
            local('rm -r {0}'.format(devf))
            local('rm -r {0}'.format(minf))

@task
def deploy():
    setup_environ()
    local('mv {0}/*.css {0}/css ### moving css files to proper directory'.format(BUILD_DIR))
    local('cp -rpu {0}/* {1} ### copying cbs files to html'.format(BUILD_DIR, DEST_DIR))
    local('cp -rpu {0}/* {1} ### copying cbs files to wordpress'.format(BUILD_DIR, WP_DIR))
    local('cat {0} {1} > {2} ### adding wordpress entry to wordpress css'.format(
        os.path.join(DIR, 'wordpress.txt'), os.path.join(WP_DIR, 'css', 'style.css'), os.path.join(WP_DIR, 'style.css')))
    local('rm -r {0}'.format(os.path.join(WP_DIR, 'css', 'style.css')))

@task
def zip_wordpress(build=True):
#    Zips wordpress directory for theme deployment
    from fabric.contrib.console import confirm
    if build == True and confirm("Do you want to run the full deployment?"):
            deploy_all()
    with settings(warn_only = True):
        local('ln -s {0} {1}/{2} ### creating properly-named symlink for zipping'.format(WP_DIR, DIR, TEMPLATE))
        local('rm wordpress.zip ### removing any previous zip files')
        local('zip -r wordpress.zip {0}/* --exclude=*.*~ ### zipping directory'.format(TEMPLATE))
        local('rm {0}/{1} ### removing properly-named symlink'.format(DIR, TEMPLATE))
    
    
    
@task
def build_bootstrap():
    with lcd(BOOTSTRAP_DIR):
        if os.path.isdir(BOOTSTRAP_RESOURCE_DIR):
            local('make clean')
        local('make bootstrap')
    local('cp -rpu {0}/* {1} ### copying bootstrap'.format(BOOTSTRAP_RESOURCE_DIR, BUILD_DIR))

def proc_vars(inputfn):
    proc = ['less', 'dev', 'min', 'css']
    return ['.'.join([inputfn.split('.', 1).pop(0), f]) for f in proc]

def setup_environ():
    if os.path.isdir(BUILD_DIR) is False:
        local('mkdir -p {0}/css'.format(BUILD_DIR))
    local('cp -rpu {0} {1}/'.format(os.path.join(DEST_DIR, 'img'), BUILD_DIR))
    local('cp -rpu {0} {1}/'.format(os.path.join(DEST_DIR, 'js'), BUILD_DIR))

