<?php
/**
    CBS Site templates
    Copyright The Regents of the University of California, Davis 
    all rights reserved
    
    Designed and built by Information & Educational Technology
    University of California, Davis
    
    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to
    deal in the Software without restriction, including without limitation the
    rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
    sell copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.

    Main footer file.

    @package WordPress
    @subpackage CBS Web Template

*/
$reg = reg();
?>
            <div id="push"><!-- this div ensures the footer sticks to the bottom of the page in content-light pages. --></div>
        </div><!-- /.container -->





        <!-- FOOTER
        ================================================== -->
        <footer>
            <div class="container">
                <div class="row">
                    <div id="colophon" class="span9">
                        <p>&copy; 
                        <a href="http://www.ucdavis.edu/">Regents of the University of California, Davis</a>. All rights reserved.</p>
                        <?php cbs_wordpress_contactinfo_display();?>
                    </div>
                    <?php if(cbs_wordpress_theme_has_social_links()):?>
                    <div id="social-media" class="span3">
                        <p>Follow Us:</p>
                        <ul class="inline">
                        <?php foreach(array_keys($reg->get('social_media')) as $type){
                            cbs_wordpress_theme_social_link($type);
                        }?>
                        </ul>
                    </div>
                    <?php endif;?>
                </div>
            </div>
        </footer>
<?php wp_footer(); ?>
</body>
</html>
