<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>
<article id="<?php cbs_wordpress_article_id(); ?>" <?php post_class(); ?>>
    <header>
        <h1>
        <?php if(!is_single()):?>
            <a href="<?php the_permalink(); ?>" 
               title="<?php echo esc_attr(sprintf(__('Permalink to %s', 'cbs_wordpress'), the_title_attribute('echo=0'))); ?>"
               rel="bookmark"><?php the_title(); ?></a>
        <?php else:?>
            <?php the_title();?>
        <?php endif;?>
        </h1>
        <?php if((!is_search() && !is_archive()) && has_post_thumbnail()):?>
        <?php the_post_thumbnail();?>
        <?php endif;?>
        <p class="byline"><?php cbs_wordpress_entry_meta();?></p>
    </header>
	<?php if ( is_search() || is_archive()) : // Only display Excerpts for Search ?>
	    <div class="entry-summary">
		    <?php the_excerpt(); ?>
	    </div><!-- .entry-summary -->
	<?php else : ?>
        <?php if(has_excerpt()):?>
            <div class="summary" id="abstract">
            <?php the_excerpt();?>
            </div>
        <?php endif; // article has excerpt ?>
	    <?php the_content(__('Continue reading <span class="meta-nav">&rarr;</span>', 'cbs_wordpress')); ?>
	    <?php wp_link_pages(array('before' => '<div class="page-links">' . __('Pages:', 'cbs_wordpress'), 'after' => '</div>')); ?>
	<?php endif; ?>
</article>
