<?php
$highlights = get_category_by_slug('highlights');
$events = get_category_by_slug('events');
if(function_exists("register_field_group"))
{
    register_field_group(array (
        'id' => 'acf_event-information',
        'title' => 'Event Information',
        'fields' => array (
            array (
                'key' => 'field_51f1a5493140b',
                'label' => 'Where',
                'name' => 'location',
                'type' => 'text',
                'required' => 1,
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'html',
                'maxlength' => '',
            ),
            array (
                'key' => 'field_52e2e08ad0565',
                'label' => 'Start date',
                'name' => 'start_date',
                'type' => 'date_time_picker',
                'required' => 1,
                'show_date' => 'true',
                'date_format' => 'M d, yy',
                'time_format' => 'h:mm tt',
                'show_week_number' => 'false',
                'picker' => 'slider',
                'save_as_timestamp' => 'true',
                'get_as_timestamp' => 'false',
            ),
            array (
                'key' => 'field_52e2e0e9e6374',
                'label' => 'End date',
                'name' => 'end_date',
                'type' => 'date_time_picker',
                'show_date' => 'true',
                'date_format' => 'M d, yy',
                'time_format' => 'h:mm tt',
                'show_week_number' => 'false',
                'picker' => 'slider',
                'save_as_timestamp' => 'true',
                'get_as_timestamp' => 'false',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'post',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
                array (
                    'param' => 'post_category',
                    'operator' => '==',
                    'value' => $events->term_id,
                    'order_no' => 1,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'side',
            'layout' => 'default',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));
    register_field_group(array (
        'id' => 'acf_feature-in-rotator',
        'title' => 'Feature in Rotator',
        'fields' => array (
            array (
                'key' => 'field_5214ed1bd2b1e',
                'label' => 'Description',
                'name' => 'description',
                'type' => 'textarea',
                'instructions' => 'Body copy for slide. Defaults to this post\'s excerpt. Create buttons with <a target="_blank" href=\'http://getbootstrap.com/2.3.2/base-css.html#buttons\'>these styles</a>',
                'default_value' => '',
                'placeholder' => '',
                'maxlength' => '',
                'formatting' => 'none',
            ),
            array (
				'key' => 'field_52f18948e719c',
				'label' => 'Custom CSS',
				'name' => 'custom_css',
				'type' => 'textarea',
				'instructions' => 'Custom CSS. Put CSS styling here to adjust how your carousel card shows up in the main rotator.',
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'formatting' => 'none',
			),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'post',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
                array (
                    'param' => 'post_category',
                    'operator' => '==',
                    'value' => $highlights->term_id,
                    'order_no' => 1,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'side',
            'layout' => 'default',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));
}

