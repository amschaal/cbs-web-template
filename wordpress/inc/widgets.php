<?php
/*
    CBS Wordpress Widget definitions
    
    Custom widgets for CBS Wordpress
*/

include(DIR . '/inc/mime_types.php');

function cbs_wordpress_register_widgets(){
    register_widget('RelatedFiles_Widget');
    register_widget('TableOfContents_Widget');
    register_widget('ACFEvents_Widget');
}
add_action('widgets_init', 'cbs_wordpress_register_widgets');
add_filter('the_content', 'cbs_wordpress_filter_header_ids');



/*******************************************************************************
    Table of contents widget
*******************************************************************************/
class TableOfContents_Widget extends WP_Widget {
    function __construct() {
        parent::__construct(
            'toc_widget',
            'Table of Contents',
            array(
                'description' => __("Displays a table of contents for the current article based on the headings.", 'cbs_wordpress'),
            )
        );
        $this->post = false;
    }
    
    public function widget($args, $instance){
        $post = $this->get_post();
        if(!$post)
            return;
        $content = apply_filters('the_content', $post->post_content);
        $elements = $this->get_headers($content);
        if(!$elements->length)
            return;

        extract($args);
        $pages = explode('<!--nextpage-->', $content);
        $title = apply_filters('widget_title', $this->name);

        $current_level = 6;

#        Find base header
        $elements = $this->get_headers($content);
        
        foreach($elements as $i => $element){
            $level = (int) substr($element->tagName, 1, 1);
            $current_level = $level < $current_level ? $level : $current_level;
        }

        echo $before_widget;
        echo $before_title . $title . $after_title;

        $out = "";
        foreach($pages as $page => $page_content){
            $elements = $this->get_headers($page_content);
            $current_page = $page + 1;
            foreach($elements as $i => $element){
                $level = (int) substr($element->tagName, 1, 1);
                if($level < $current_level){
                    $out .= str_repeat("</ul>\n", ($current_level - $level));
                }
                if($level > $current_level){
                    $out .= str_repeat("<ul>\n", ($level - $current_level));
                }
                if($element->hasAttribute('id'))
                    $prepend = $current_page === 1 ? 
                        get_permalink($post->ID) . "#" : 
                        get_permalink($post->ID) . "$current_page/#";
                    $out .= sprintf("<li><a href='%s'>%s</a></li>\n",
                        $prepend . $element->getAttribute('id'), 
                        $element->nodeValue
                    );
                $current_level = $level;
            }
        }
        echo $out;
        echo $after_widget;
    }

    private function get_post(){
        if(!$this->post)
            $this->post = get_post($GLOBALS['post']->ID) or false;
        return $this->post;
    }
    public static function get_headers($content){
        $dom = new DOMDocument;
        $dom->formatOutput = true;
        //  we must convert the encoding since DOMDocument doesn't use UTF8
        //  used by WP as default
        $dom->loadHTML(mb_convert_encoding($content, 'HTML-ENTITIES', get_bloginfo('charset')));
        $xpath = new DOMXPath($dom);
        $expression = '
    (
        //h1|//h2|//h3|//h4|//h5|//h6
    )';
        return $xpath->query($expression);
    }
}

function cbs_wordpress_header_id($value){
    global $post;
    return sanitize_title($post->post_title . ' ' . $value);
}

function cbs_wordpress_filter_header_ids($content){
    $elements = TableOfContents_Widget::get_headers($content);
    if($elements->length):
        foreach ($elements as $index => $element) {
            if(!$element->hasAttribute('id'))
                $element->setAttribute('id', cbs_wordpress_header_id($element->nodeValue));
            $element->setAttribute('class', trim(join(' ', array($element->getAttribute('class'), 'post-heading'))));
        }
        $content = $element->ownerDocument->saveHTML();
    endif;
    return $content;
}



/*******************************************************************************
    Related files widget
*******************************************************************************/
class RelatedFiles_Widget extends WP_Widget {
    function __construct(){
        $this->defaults = array(
            'title'         => __('Files', 'cbs_wordpress'),
            'file_types'    => false,
        );
        $this->file_types = $this->defaults['file_types'];
        parent::__construct(
            'related_files_widget', 
            $this->defaults['title'], 
            array(
            'description' => __("Lists a post's related files", 'cbs_wordpress' ),
            )
        );
    }

    public function widget($args, $instance){
        extract($args);
        $file_types = $this->get_related_files($instance['file_types']);
        if(!$file_types)
            return;
        echo $args['before_widget'];
        $title = apply_filters('widget_title', $this->name);
        echo $args['before_title'] . $title . $args['after_title'];
        ?>
            <ul>
            <?php
            foreach($file_types as $attachment):?>
            <li id="file-<?php echo $attachment->ID;?>">
            <a href="<?php echo $attachment->guid;?>"
            class="attachment <?php echo esc_html($attachment->post_mime_type);?>">
            <?php echo apply_filters('the_title', $attachment->post_title);?>
            </a></li>
            <?php 
            endforeach; ?>
            </ul>
        <?php
        echo $args['after_widget'];
    }

    public function form($instance){
        $instance = wp_parse_args((array) $instance, $this->defaults);
        $exclude = esc_attr( $instance['exclude'] );
        $file_types = esc_attr($this->sanitize_mime_type_list($instance['file_types']));
        ?>
        <p>
        <label for="<?php echo $this->get_field_name('file_types'); ?>"><?php _e('Show these file types:'); ?></label> 
        <input  class="widefat" 
                id="<?php echo $this->get_field_id('file_types'); ?>" 
                name="<?php echo $this->get_field_name('file_types'); ?>" 
                type="text" value="<?php echo $file_types; ?>" />
        <br />
        <small><?php _e( 'File types, separated by comma (i.e. "doc, pdf")<br/>Leave blank to show all attatched files.' ); ?></small>
        </p>
        <?php 
    }

    public function update($new, $old){
        $instance = $old;
        $instance['file_types'] = $this->sanitize_mime_type_list($new['file_types']);
        return $instance;
    }

    private function get_related_files($files){
        $featured_image = get_post_thumbnail_id($GLOBALS['post']->ID);
        $args = array(
            'post_type' => 'attachment', 
            'numberposts' => -1, 
            'post_status' => null, 
            'post_parent' => $GLOBALS['post']->ID,
        );
        if(!empty($files))
            $args['post_mime_type'] = mime_types_map(explode(", ", $files));
        if(!empty($featured_image))
            $args['exclude'] = $featured_image;
        $attachments = get_posts($args);
        return $attachments;
    }
    
    private function sanitize_mime_type_list($user_input){
        $input = explode(",", $user_input);
        $input = array_map('trim', $input);
        $input = array_map('sanitize_mime_type', $input);
        $func = function($value){
            return in_array($value, explode("|", implode("|", array_keys(get_allowed_mime_types())))) ? $value : false;
        };
        $input = array_map($func, $input);
        return implode(", ", array_filter($input));
    }
}



/*******************************************************************************
    Events Widget
*******************************************************************************/
class ACFEvents_Widget extends WP_Widget {

    var $start_date = false;
    var $end_date = false;

    var $lk = 'location';
    var $sk = 'start_date';
    var $ek = 'end_date';

    var $cat = 'F j, Y';
    var $tf = 'g:ia';
    var $hf = 'H:i';
    var $df = 'M j, Y g:ia'; 
    var $sf = 'M j, Y';

    function __construct(){
        $this->defaults = array(
            'title'             => __('Events', 'cbs_wordpress'),
            'events_to_display' => 3,
        );
        parent::__construct(
            'cbs_wordpress_events_widget', 
            $this->defaults['title'], 
            array(
            'description' => __("Lists all the upcoming events for the organization. 
            Requires the 'Advanced Custom Fields' plugin.", 'cbs_wordpress' ),
            )
        );
        $this->date_fields = $this->date_fields();
    }

    public function widget($args, $instance){
        extract($args);
        $events = wp_cache_get($this->id_base) ? wp_cache_get($this->id_base) : array();
        if(empty($events)):
            $events_to_display = esc_attr($instance['events_to_display']);
            $events_to_display = empty($events_to_display) ? -1 : $events_to_display;
            $event_category = absint($instance['event_category']);
            
            //  start date query
            $meta_query = array(
                'relation' => 'OR',
                array(
                'key' => $this->sk,
                'value' => strtotime('today midnight'),
                'compare' => '>=',
                'type' => 'NUMERIC',
                ),
                array(
                'key' => $this->ek,
                'value' => time(),
                'compare' => '>=',
                'type' => 'NUMERIC',
                ),
            );
            $query_args = array(
                'category_name' => 'events',
                'posts_per_page' => $events_to_display,
                'meta_query' => $meta_query,
            );
            $query = new WP_Query($query_args);
            while($query->have_posts()){
                $event = $query->next_post();
                $id = get_field($this->sk, $event->ID);
                $events[$id] = $event;
            }
            wp_cache_set($this->id_base, $events, false, 3600);
            wp_reset_postdata();
        endif;
        sort($events);

        $cat_link = get_category_link($event_category);
        
#        output title bar
        echo $before_widget;
        ?>
        <h1 class="section-heading"><?php echo $this->name;?></h1>
        <a class="rss-link" href="<?=add_query_arg(array('feed' => 'rss2'), $cat_link);?>" alt="RSS feed">
        <img src="<?php _i();?>/img/rss.png" alt="Link to events RSS feed"></a>
        <?php
        if(count($events) > 0):
        ?>
        <a href="<?=$cat_link;?>" class="more-link">All Events &rarr;</a>
        <?php
        
#        loop through upcoming events
        foreach($events as $post){
        $this->start_date = get_field($this->sk) or false;
        $this->end_date = get_field($this->ek) or false;
        ?>
        <article id="events-<?=$post->ID;?>" class="events-summary">
            <header>
                <a href="<?=the_permalink();?>"><h2><?=the_title();?></h2></a>
            </header>
            <dl class="event-details dl-horizontal">
                <dt>When:</dt>
                <dd><?=$this->format_date();?></dd>
                <dt>Where:</dt>
                <dd><?=get_field($this->lk);?></dd>
            </dl>
            <?=apply_filters('the_excerpt', wp_trim_words($post->post_content));?>
        </article>
        <?php     
        }?>
        <a class="more-link" href="<?=$cat_link;?>">More events &rarr;</a>
        <?php
        else:
        ?>
        <p>No new events at this time.</p>
        <?php
        endif;
        ?>
        <?php
#        output footer
        echo $after_widget;
    }
    
    public function form($instance){
        $instance = wp_parse_args((array) $instance, $this->defaults);
        $etd = esc_attr($instance['events_to_display']);
        $cats = get_categories();
        ?>
        <p>
        <label for="<?php echo $this->get_field_name('events_to_display'); ?>"><?php _e('Number of events:'); ?></label> 
        <input  class="widefat" 
                id="<?php echo $this->get_field_id('events_to_display'); ?>" 
                name="<?php echo $this->get_field_name('events_to_display'); ?>" 
                type="text" value="<?php echo $etd; ?>" />
        <br />
        <small><?php _e('Number of events to show in the widget.<br/>Leave blank to show all upcoming dates.'); ?></small>
        </p>
        <p>
        <label for="<?php echo $this->get_field_id('event_category'); ?>"><?php _e( 'Event category:' ); ?></label>
        <select name="<?php echo $this->get_field_name('event_category'); ?>" 
                id="<?php echo $this->get_field_id('event_category'); ?>" class="widefat">
            <?php foreach($cats as $c):?>
            <option value="<?php echo $c->cat_ID;?>"<?php selected( $instance['event_category'], $c->cat_ID ); ?>>
            <?php _e($c->name); ?>
            </option>
	        <?php endforeach;?>
        </select>
        </p>
        <?php 
    }
    
    public function update($new, $old){
        $instance = $old;

#        Events to display
        $instance['events_to_display'] = empty($new['events_to_display']) ? "" : absint($new['events_to_display']);

#        Event categories
        $cats = get_all_category_ids();
        if(empty($cats))
            return new WP_Error('no_cats_defined', __("No categories defined. At least one category is required", 'cbs_wordpress'));
        $instance['event_category'] = !is_numeric($new['event_category']) ? 1 : absint($new['event_category']);
        if(!in_array($instance['event_category'], $cats))
            $instance['event_category'] = $cats[0];

        return $instance;
    }

    private function format_date(){
        if(!$this->start_date)
            return;
        $end_date = false;
        $sdc = @date($this->sf, strtotime($this->start_date));
        $edc = @date($this->sf, strtotime($this->end_date));
        if($this->end_date)
            $end_date = $sdc == $edc ? @date($this->tf, strtotime($this->end_date)) : $this->scrub_date($this->end_date);
        $out = $this->scrub_date($this->start_date);
        if($end_date)
            $out .= " to " . $end_date;
        return $out;
    }
    
    public static function date_fields(){
        extract(get_class_vars('ACFEvents_Widget'));
        return array($sk, $ek);
    }

    public static function scrub_date($date, $format = false){
        $date = is_numeric($date) ? $date : strtotime($date);
        if(empty($date))
            return '';
        extract(get_class_vars('ACFEvents_Widget'));
        if(!$format)
            $format = @date($hf, $date) != '00:00' ? $df : $sf;
        return @date($format, $date);
    }
}

function cbs_wordpress_events_widget_validate_post($post_id){
    global $post;
    extract(get_class_vars('ACFEvents_Widget'));
    $pull_fields = function($field){return $field['key'];};
    $map = array_map($pull_fields, get_field_objects());
    $fields = isset($_POST['fields']) ? $_POST['fields'] : false;

    $fields[$map[$ek]] = strtotime($fields[$map[$ek]]) > strtotime($fields[$map[$sk]]) ? $fields[$map[$ek]] : '';
    
    $_POST['fields'] = $fields;
    return $fields;
}

foreach(ACFEvents_Widget::date_fields() as $field){
    add_filter("acf/load_value/name=$field", 'ACFEvents_Widget::scrub_date');
}
add_action('acf/save_post', 'cbs_wordpress_events_widget_validate_post', 5);
