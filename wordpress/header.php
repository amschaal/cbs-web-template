<?php
/**
    CBS Site templates
    Copyright The Regents of the University of California, Davis 
    all rights reserved
    
    Designed and built by Information & Educational Technology
    University of California, Davis
    
    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to
    deal in the Software without restriction, including without limitation the
    rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
    sell copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.

    Main header file.

    @package WordPress
    @subpackage CBS Web Template

*/
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo( 'charset' ); ?>" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!--Title, description & author of your site. Fill this in.-->
        <title><?php wp_title( '|', true, 'right' ); ?></title>
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
        <meta name="description" content="<?php bloginfo( 'description' ); ?>">
        <meta name="author" content="">

        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php _i();?>/img/icon/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php _i();?>/img/icon/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php _i();?>/img/icon/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="<?php _i();?>/img/icon/apple-touch-icon-57-precomposed.png">
        <link rel="shortcut icon" href="<?php _i();?>/img/icon/favicon.png">

    <?php wp_head(); ?>
    </head>



    <body <?php body_class(); ?>>
        <div id="page-content" class="container">
            <header>



                <!-- BRAND
                ================================================== -->
                <div class="row main-header <?=cbs_wordpress_carousel_logo_class();?>">
                    <div class="span4 logo-heading">
                        <a  href="<?php echo esc_url( home_url( '/' ) ); ?>" 
                            title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" 
                            rel="home">
                            <h1>
				<img src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="" />
			    </h1>
                        </a>
                    </div>
                    <?php if(get_option("cbs_wordpress_title_display", false)):?>
                    <div class="span8 department-heading">
                        <a href="<?=bloginfo('home');?>">
                            <h2 class="subheading"><?=bloginfo('description');?></h2>
                            <h2 class="section-name"><?=bloginfo('name');?></h2>
                        </a>
                    </div>
                    <?php endif;?>
                </div>                
                


                
                <!-- NAVBAR
                ================================================== -->
                <nav class="navbar <?=cbs_wordpress_carousel_nav_class();?>" role="navigation">
                    <div class="navbar-inner">
                        <div class="container">
                            <button id="main-nav-btn" type="button" class="btn btn-navbar" 
                            data-toggle="collapse" data-target=".nav-collapse">
                                <i class="icon-list"></i>
                            </button>
                            <div class="nav-menu nav-collapse collapse">
                                <a  class="assistive-text" 
                                    href="#content" 
                                    title="<?php esc_attr_e( 'Skip to content', 'cbs_wordpress' ); ?>">
                                    <?php _e( 'Skip to content', 'cbs_wordpress' ); ?>
                                </a>
                                <?php cbs_wordpress_main_nav(); ?>
                            </div><!--/.nav-collapse -->
                            <?php get_search_form();?>
                        </div><!-- /.container -->
                    </div><!-- /.navbar-inner -->
                </nav><!-- /.navbar -->



            <?php if(cbs_wordpress_show_carousel()): ?>
                <!-- CAROUSEL
                ================================================== -->
                <div id="myCarousel" class="carousel slide">
                    <div class="carousel-inner">
                <?php
                $options = cbs_wordpress_carousel_options();
                if($options['rotator_type'] == 'gallery' && !empty($options['gallery_shortcode'])):
                    echo do_shortcode($options['gallery_shortcode']);
                elseif($options['rotator_type'] == 'posts'):
                    $carousel = cbs_wordpress_get_carousel();
                    $counter = 0;
                    foreach($carousel as $key => $c):
                    $counter++;
                    $active = $counter === 1 ? "active" : "";
                    ?>
                        <div id="carousel-<?=$key;?>" class="item item-<?=sprintf('%1$02d', $counter);?> <?=$active;?>">
                            <?=$c['img'];?>
                            <div class="container">
                                <div class="carousel-caption">
                                    <h2><?=$c['title'];?></h2>
                                    <p class="lead"><?=$c['description'];?></p>
                                </div>
                            </div>
                        </div>
                    <?php endforeach;?>
                    </div>
                    <?php if($counter > 1):?>
                    <a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>
                    <a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>
                    <?php endif;?>
                <?php endif;?>
                </div><!-- /.carousel -->
            <?php endif;?>
            </header>
                
                


