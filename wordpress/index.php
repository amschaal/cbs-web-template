<?php
/*
    CBS Site templates
    Copyright The Regents of the University of California, Davis 
    all rights reserved
    
    Designed and built by Information & Educational Technology
    University of California, Davis
    
    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to
    deal in the Software without restriction, including without limitation the
    rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
    sell copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
    
    The main template file.

    This is the most generic template file in a WordPress theme
    and one of the two required files for a theme (the other being style.css).
    It is used to display a page when nothing more specific matches a query.
    E.g., it puts together the home page when no home.php file exists.
    Learn more: http://codex.wordpress.org/Template_Hierarchy

    @package WordPress
    @subpackage CBS Web Template

*/
get_header(); ?>
    <div class="content row">
        <section id="article" class="<?php cbs_wordpress_theme_columns();?>">
        <?php if ( have_posts() ) : ?>
            <?php /* Start the Loop */ ?>
            <?php while ( have_posts() ) : the_post(); ?>
                <?php get_template_part( 'content', get_post_format() ); ?>
            <?php endwhile; ?>
        <?php else : ?>
        <article id="post-0" class="post no-results not-found">
	        <header class="entry-header">
		        <h1 class="entry-title"><?php _e( 'No posts to display', 'cbs_wordpress' ); ?></h1>
	        </header>
	        <div class="entry-content">
	        <p>
            <?php if ( current_user_can( 'edit_posts' ) ) :
                    // Show a message to people who can edit posts
                ?>
                <?php printf(__('No results found. <a href="%s">Publish your first post</a>.', 'cbs_wordpress' ), 
                    admin_url('post-new.php')); ?>
                <?php else :
	                // Show the default message to everyone else.
                ?>
                <?php _e('No results found. Try the search form above for best results.', 'cbs_wordpress'); ?>				
            <?php endif; // end current_user_can() check ?>
            </p>
	        </div><!-- .entry-content -->
        </article><!-- #post-0 -->
        <?php endif; // end have_posts() check ?>
        </section><!-- /.News -->
        <?php get_sidebar(); ?>
    </div>
<?php get_footer();
